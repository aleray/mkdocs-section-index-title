# MKDocs Section Index Title Plugin

A plugin for [MKDocs](https://www.mkdocs.org/) that allows you to customize section titles in the navigation based on metadata defined in the Markdown files when using [MKDocs Section Index plugin](https://github.com/oprypin/mkdocs-section-index).

## Installation

```bash
pip install mkdocs-section-index-title-plugin
```

## Usage

Add the following lines to your `mkdocs.yml` file:

```yaml
plugins:
  - section_index_title
```
Set `modify_titles` to `false` to disable title modification

```yaml
plugins:
  - section_index_title:
      modify_titles: false
```

## Dependencies

This plugin requires the [mkdocs-section-index](https://github.com/zhaoterryy/mkdocs-section-index) Mkdocs extension.

## License

Yet to be defined
