from setuptools import setup, find_packages

setup(
    name="mkdocs_section_index_title",
    version="0.0.1",
    description='Derive titles from metadata when using mkdocs-section-index',
    url="https://tangible-cloud.be/",
    author="Alex",
    author_email="alexandre@stdin.fr",
    packages=find_packages(),
    install_requires=[
        'mkdocs>=1.5',
        'mkdocs-section-index>=0.3.0',
    ],
    entry_points={
        "mkdocs.plugins": [
            'section_index_title = mkdocs_section_index_title.plugin:SectionIndexTitlePlugin',
        ]
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
    ],
)
