from mkdocs.config import config_options
from mkdocs.plugins import BasePlugin


class SectionIndexTitlePlugin(BasePlugin):
    # Define the configuration options for the plugin.
    config_scheme = (
        ('modify_titles', config_options.Type(bool, default=True)),
    )

    def on_nav(self, nav, config, files, **kwargs):
        # Check if title modification is enabled in the configuration.
        if self.config['modify_titles']:
            # If enabled, call the method to modify section titles.
            self.modify_section_titles(nav, config)
        return nav

    def modify_section_titles(self, nav, config):
        # Iterate through the navigation items.
        for item in nav:
            # Check if the item is a page and a section.
            if item.is_page and item.is_section:
                # Read the source file to access metadata.
                item.read_source(config)
                # Update the title with the 'title' metadata if available.
                item.title = item.meta.get('title', item.title)
                # If the section has children, recursively modify their titles.
                if item.children:
                    self.modify_section_titles(item.children, config)
